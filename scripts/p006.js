"use strict";

const max = 100 , sum = max*(max+1)/2;
let total=0;

for(let i=1 ; i<=max ; i++){
	total+= i * (sum-i);
}

/*
for (let i=1;i<max;i++){
	for(let j=i+1;j<=max;j++){
		total+=2*j*i
	}
}
*/

console.log(total);