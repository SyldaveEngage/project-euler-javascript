"use strict";

let increment=1, triangular=1;

while(true){
	triangular += ++increment;
	if( divisorCount(triangular)>=500 ){
		break;
	}
}

function divisorCount(num){
	let div=num , divisors=[] , dNumber=1;
	while(div>1){
		let i=2;
		
		while(div%i!=0){
			i++;
		}
		
		if(divisors.length>0 && i===divisors[divisors.length-1][0]){
			divisors[divisors.length-1][1]++;
		}else{
			divisors.push([i,1]);
		}
		
		div/=i;
		
	}
	for(let j=0;j<divisors.length;j++){
		dNumber*=(divisors[j][1]+1);
	}
	return dNumber;
}

console.log(triangular);