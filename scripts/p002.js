"use strict";

let n1=1,n2=2,num=0;

while(n2<=4000000){
	if(n2%2==0){
		num+=n2;
	}
	n2+=n1;
	n1=n2-n1;
}

console.log(`${num}  ,  ${n1}  ;  ${n2}`);