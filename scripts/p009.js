"use strict";

const sum=1000;
let a,b,c;

function pythagoreanSum(sum){
	for(a=1 ; a<sum ; a++){
		for(b=1 ; b<a ; b++){
			
			c = Math.sqrt(a**2 + b**2);
			console.log(`${a} ; ${b} ; ${c}`);
			
			if(  c%1==0 && a+b+c===sum ){
				return [a, b, c, a*b*c];
			}
		}
	}
}

let ans = pythagoreanSum(sum);

console.log(`${ans[0]} * ${ans[1]} * ${ans[2]} = ${ans[3]}`);