"use strict";

const min=100,max=999;
let largestP=0;

for(let i=max;i>=min;i--){
	for(let j=i;j>=min;j--){
		if( i*j>largestP && isPalindrome(j*i) ){
			largestP=i*j;
		}
	}
}

console.log(largestP);

function isPalindrome(num){
	
	let reversed="";
	let tmp=num+"";

	for(let i=tmp.length-1 ; i>=0 ; i--){
		reversed+=tmp[i];
	}

	if(parseInt(reversed)===num){
		return true;
	}
	
	return false;
}

function log10(num){
	return log(num)/log(10);
}