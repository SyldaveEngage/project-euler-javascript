'use strict';

let max = 10000;
let amicables = [] , amicablesSum = 0;

for(let i=1 ; i<max ; i++){
    if(!isAmicable(i) && divisorsSum(divisorsSum(i))===i){
        (divisorsSum(i)===i) ? 0 : amicables.push(i , divisorsSum(i));
    }
}

for(let i=0 ; i<amicables.length ; i++){
    amicablesSum += amicables[i];
}

console.log(amicablesSum);

function isAmicable(number){
    for(let i=0 ; i<amicables.length ; i++){
        if(amicables[i]===number){
            return true;
        }
    }
    return false;
}

function divisorsSum(number){
    let divisors = [1];
    let sum = 0;
    let loopEnd = Math.floor(Math.sqrt(number));

    for(let i=2 ; i<=loopEnd ; i++){
        if(number%i === 0){
            divisors.push(i);
            (number/i !== i) ? divisors.push(number/i):0;
        }
    }

    for(let i=0 ; i<divisors.length ; i++){
        sum += divisors[i];
    }

    return sum;
}