"use strict";

const divisorMax = 20;
let smallestMultiple=1;

for(let i=2 ; i<=divisorMax ; i++){
	smallestMultiple = ppcm(smallestMultiple,i);
}

console.log(smallestMultiple);

function ppcm(n1,n2){
	let pgcd;
	let a=n1 , b=n2;
	
	while(a * b){
		if(a>b){
			a %= b;
		}else{
			b %= a;
		}
	}
	
	if(a){
		pgcd = a;
	}else{
		pgcd = b;
	}
	
	return n1 * n2 / pgcd;
	
}