"use strict";

let letterCount , totalLetterCount = 0;

for(let i=1 ; i<=1000 ; i++){
	letterCount = 0;

	switch( i%100 ){
		case 1:
		case 2:
		case 6:
		case 10:
			letterCount+=3;
			break;
		case 4:
		case 5:
		case 9:
			letterCount+=4;
			break;
		case 3:
		case 7:
		case 8:
			letterCount+=5;
			break;
		case 11:
		case 12:
			letterCount+=6;
			break;
		case 15:
		case 16:
			letterCount+=7;
			break;
		case 13:
		case 14:
		case 18:
		case 19:
			letterCount+=8;
			break;
		case 17:
			letterCount+=9;
			break;
		default:
			switch( (i%100-i%10)/10 ){
				case 4:
				case 5:
				case 6:
					letterCount+=5;
					break;
				case 2:
				case 3:
				case 8:
				case 9:
					letterCount+=6;
					break;
				case 7:
					letterCount+=7;
					break;
				default:
			}
			switch( i%10 ){
				case 1:
				case 2:
				case 6:
					letterCount+=3;
					break;
				case 4:
				case 5:
				case 9:
					letterCount+=4;
					break;
				case 3:
				case 7:
				case 8:
					letterCount+=5;
					break;
				default:
			}
	}
	
	
	switch( Math.floor(i/100) ){
		case 0:
			break;
		case 1:
		case 2:
		case 6:
			letterCount+=10;
			break;
		case 3:
		case 7:
		case 8:
			letterCount+=12;
			break;
		case 4:
		case 5:
		case 9:
			letterCount+=11;
			break;
		case 10:
			letterCount+=11;
			break;
		default:
	}

	if(i%100!==0 && Math.floor(i/100) !== 0){
		letterCount+=3;
	}

	totalLetterCount += letterCount;
	console.log(`${i} : ${letterCount}`)

}

console.log(totalLetterCount);