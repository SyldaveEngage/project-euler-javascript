"use strict";

const months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const startingYear = 1901;
const endingYear = 2000;

let today = 2;
let countingSundays = 0;

for(let year=startingYear ; year<=endingYear ; year++){
    for(let i=0; i<12 ; i++){

        if(today%7===0){
            countingSundays++;
        }

        if(i===1 && year%4===0 && (year%100!==0 || year%400===0)){
            today+=29;
        }else{
            today+=months[i];
        }
    }
}

console.log(countingSundays);