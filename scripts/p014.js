"use strict";

const max=1000000;
let longestChain=0 , tmpChain , tmpNode , startingNumber;
let chains = [];

for(let i=1 ; i<max ; i++){
	tmpChain = 1;
	tmpNode = i;

	while(tmpNode!=1){

		if(tmpNode%2===0){
		
			tmpNode /= 2;
			if(tmpNode<i){
				tmpChain += chains[tmpNode-1][1];
				break;
			}
		
		}else{
		
			tmpNode = 3*tmpNode+1;
		
		}
		tmpChain++;
	}
	chains.push([i,tmpChain]);

	if(tmpChain>longestChain){
		longestChain = tmpChain;
		startingNumber = i;
		console.log(`Plus longue chaîne : ${startingNumber} : ${longestChain}`);
	}
}

console.log(`Plus longue chaîne : ${startingNumber} : ${longestChain}`);