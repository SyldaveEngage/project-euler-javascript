"use strict";

const max=2000000;
let primes=[2] , isPrime , primesSum=0 , primesLength;

for(let i=3;primes[primes.length-1]<max;i++){
	isPrime=true;
	for(let j=0;j<primes.length;j++){
		if(i%primes[j]==0){
			isPrime=false;
			break;
		}
	}
	if(isPrime){
		primes.push(i);
	}
}

primesLength=primes.length;

for(let i=0;i<primesLength-1;i++){
	primesSum+=primes[i];
}

console.log(primesSum);