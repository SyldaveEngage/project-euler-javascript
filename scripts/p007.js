"use strict";

const primes=[2];
let isPrime;

for(let i=3;primes.length!=10001;i++){
	isPrime=true;
	for(let j=0;j<primes.length;j++){
		if(i%primes[j]==0){
			isPrime=false;
			break;
		}
	}
	if(isPrime){
		primes.push(i);
	}
}

console.log(primes[primes.length-1]);