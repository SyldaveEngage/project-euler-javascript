"use strict";

let total = 0;
let abundants = [] , divisors = [];
let limit = 28123;

for(let i=1 ; i<limit ; i++){
    if(isAbundant(i)){
        abundants.push(i);
    }
    if(!isSumOfAbundants(i)){
        total += i;
    }
}

console.log(total);

function isSumOfAbundants(number){
    for(let i=0 ; i<abundants.length ; i++){
        for(let j=i ; j<abundants.length ; j++){
            if(i+j == number){
                return true;
            }
        }
    }
    return false;
}

function isAbundant(number){
    return (divisorsSum(number)-number > 0);
}

function divisorsSum(number){
    let divisors = [1];
    let sum = 0;
    let loopEnd = Math.floor(Math.sqrt(number));

    for(let i=2 ; i<=loopEnd ; i++){
        if(number%i === 0){
            divisors.push(i);
            (number/i !== i) ? divisors.push(number/i):0;
        }
    }

    for(let i=0 ; i<divisors.length ; i++){
        sum += divisors[i];
    }

    return sum;
}