const startSum=200,coins=[200,100,50,20,10,5,2,1],coinLength=coins.length;
var nos = caseCount(startSum,0);//number of solutions
let text=document.getElementById('text');

function caseCount(amount,largeCoinIndex){
	
	if(amount<0){
		return 0;
	}else if(amount==0){
		return 1;
	}
	
	let count=0;
	
	for(let i=largeCoinIndex;i<coinLength;i++){
		count+= caseCount(amount-coins[i],i);
	}
	
	return count;
}

text.innerHTML+='\n'+nos;