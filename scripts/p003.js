"use strict";

const num=600851475143;
let factor, largest=0, decreasing=num;

while(decreasing>2){
	factor = 2;
	while(true){
		if(decreasing % factor == 0){
			decreasing /= factor;
			if(factor>largest){
				largest = factor;
			}
			break;
		}
		factor++;
	}
}

console.log(largest);